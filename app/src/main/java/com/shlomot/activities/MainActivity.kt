package com.shlomot.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.shlomot.R
import com.shlomot.fragments.MovieDetailsFragment
import com.shlomot.utils.FragmentsUtils
import com.shlomot.fragments.MoviesRcvFragment
import com.shlomot.fragments.ViewpagerFragment
import com.shlomot.interfaces.OnMovieClickListener
import com.shlomot.models.MovieRoomModel
import com.shlomot.room_database_components.MovieViewModel




class MainActivity : AppCompatActivity() , OnMovieClickListener {
    var movies: ArrayList<MovieRoomModel> = ArrayList()

    var rcvFragment = MoviesRcvFragment.newInstance()
//    var pagerFragment = ViewpagerFragment.newInstance(0)

    companion object {
        @JvmStatic
        fun startActivity(activity: Activity) = MainActivity().apply {
            val intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            FragmentsUtils.add(
                supportFragmentManager,
                R.id.AM_main_framelayout,
                rcvFragment,
                null,false)

    }

    override fun onResume() {
        super.onResume()
        configureViewModel()
    }

    private fun configureViewModel() {
            var movieViewModel = MovieViewModel(application)
            movieViewModel.getAllMovies().observe(this@MainActivity, Observer {
                if (it != null && !it.isEmpty()) {
                    movies.clear()
                    movies.addAll(it)
                        rcvFragment.updateMovies(movies)
                }
            })
    }


    override fun onMovieClicked(movies: ArrayList<MovieRoomModel>?, index: Int) {
        val fragment = ViewpagerFragment.newInstance(this.movies, index)
        FragmentsUtils.add(supportFragmentManager, R.id.AM_main_framelayout, fragment,null,true)

    }
}