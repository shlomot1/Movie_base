package com.shlomot.activities

import android.os.Bundle
import android.util.Log
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.shlomot.R
import com.shlomot.interfaces.OnCompletionListener
import com.shlomot.models.ApiError
import com.shlomot.models.MovieRetrofitModel
import com.shlomot.models.MovieRoomModel
import com.shlomot.retrofit_components.RequestManager
import com.shlomot.room_database_components.MoviesRepository
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "SplashActivity"

class SplashActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        AS_logo.startAnimation(getAnimation())

        downloadMovies()
    }

    private fun getAnimation(): Animation {
        val animation = AlphaAnimation(0.00f, 1.00f)

        animation.duration = 2000
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {}

            override fun onAnimationRepeat(p0: Animation?) {}

            override fun onAnimationEnd(p0: Animation?) {
                MainActivity.startActivity(this@SplashActivity)
            }
        })
        return animation
    }

    private fun downloadMovies() {
            RequestManager.getMovies(object : OnCompletionListener<List<MovieRetrofitModel>> {

                override fun onSuccess(result: List<MovieRetrofitModel>) {
                    storeMoviesAtDB(result)
                }

                override fun onFailure(error: ApiError) {
                    Log.e(TAG, "onFailure: ${error.getMessage()}")
                    Toast.makeText(this@SplashActivity, error.getMessage(), Toast.LENGTH_SHORT).show()
                }
            })
        }


    fun storeMoviesAtDB(movies: List<MovieRetrofitModel>) {
        val coroutineScope = CoroutineScope(Dispatchers.IO)
        coroutineScope.launch {
            val roomModelList = ArrayList<MovieRoomModel>()
            for (i in movies.indices) {
                roomModelList.add(MovieRoomModel(movies[i]))
            }

            val repository = MoviesRepository(application)
            repository.insert(roomModelList)
            openMainActivity()
        }
    }

    suspend fun openMainActivity() {
        withContext(Dispatchers.Main) {
            MainActivity.startActivity(this@SplashActivity)
        }
    }
}