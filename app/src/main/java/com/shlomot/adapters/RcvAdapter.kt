package com.shlomot.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.shlomot.R
import com.shlomot.interfaces.OnMovieClickListener
import com.shlomot.models.MovieRoomModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details_movie.view.*
import kotlinx.android.synthetic.main.item_movie.view.*

class RcvAdapter( movies: ArrayList<MovieRoomModel>, listener: OnMovieClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mListener: OnMovieClickListener
    var mMovies: ArrayList<MovieRoomModel> = ArrayList()



    init {
        this.mListener = listener
        mMovies = movies
        notifyDataSetChanged()
    }

    fun setMovies(movies: ArrayList<MovieRoomModel>) {
        this.mMovies = movies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        return MovieVH(inflater.inflate(R.layout.item_movie,parent,false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MovieVH).onBindViewHolder(mMovies[position])

        holder.itemview.setOnClickListener {
            mListener?.onMovieClicked(null, position)
        }
    }

    override fun getItemCount(): Int {
        return mMovies.size
    }

    class MovieVH(itemview : View): RecyclerView.ViewHolder(itemview) {

        var picasso: Picasso
        var itemview: View

        var poster : ImageView
        var title : TextView
        var released : TextView

        init {
            this.itemview = itemview
            picasso = Picasso.get()
            poster = itemView.IM_poster
            title = itemview.IM_tv_title
            released = itemview.IM_tv_date
        }

        fun onBindViewHolder(movie: MovieRoomModel){
            title.text = movie.title
            released.text = movie.releaseYear.toString()

            picasso.load(movie.image)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.default_poster_ico)
                .into(poster)

        }
    }


}