package com.shlomot.models;

import android.os.Parcel;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.TypeConverters;

import com.shlomot.room_database_components.MovieViewModel;
import com.shlomot.utils.GenresConverter;

import java.util.List;

@Entity(tableName = "movie_table", primaryKeys = {"title"})
public class MovieRoomModel extends BaseMovieModel {

    private static MovieViewModel INSTANCE;

    public MovieRoomModel(String title, String image, Double rating, Integer releaseYear, Genres genres){
        super(title,image,rating,releaseYear);
        this.genres = genres;
    }

    @Ignore
    public MovieRoomModel(String title, String image, Double rating, Integer releaseYear, List<String> genres){
        super(title,image,rating,releaseYear);
        this.genres = new Genres(genres);
    }

    @Ignore
    public MovieRoomModel(MovieRetrofitModel retrofitModel){
        super(retrofitModel.getTitle(),retrofitModel.getImage(),retrofitModel.getRating(),retrofitModel.getReleaseYear());
        this.genres = new Genres(retrofitModel.getGenre());
    }

//    @PrimaryKey(autoGenerate = true)
    int id;

    @TypeConverters(GenresConverter.class)
    private Genres genres;

    public Genres getGenres() {
        return genres;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setGenres(Genres genres) {
        this.genres = genres;
    }

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.id);
        dest.writeParcelable(this.genres, flags);
    }

    @Ignore
    protected MovieRoomModel(Parcel in) {
        super(in);
        this.id = in.readInt();
        this.genres = in.readParcelable(Genres.class.getClassLoader());
    }

    @Ignore
    public static final Creator<MovieRoomModel> CREATOR = new Creator<MovieRoomModel>() {
        @Override
        public MovieRoomModel createFromParcel(Parcel source) {
            return new MovieRoomModel(source);
        }

        @Override
        public MovieRoomModel[] newArray(int size) {
            return new MovieRoomModel[size];
        }
    };
}
