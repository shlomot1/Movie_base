package com.shlomot.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieRetrofitModel extends BaseMovieModel {

    public MovieRetrofitModel(String title, String image, Double rating, Integer releaseYear, List<String> genre){
        super(title,image,rating,releaseYear);
        this.genre = genre;
    }

    @SerializedName("genre")
    private List<String> genre;

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringList(this.genre);
    }


    protected MovieRetrofitModel(Parcel in) {
        super(in);
        this.genre = in.createStringArrayList();
    }


    public static final Creator<MovieRetrofitModel> CREATOR = new Creator<MovieRetrofitModel>() {
        @Override
        public MovieRetrofitModel createFromParcel(Parcel source) {
            return new MovieRetrofitModel(source);
        }

        @Override
        public MovieRetrofitModel[] newArray(int size) {
            return new MovieRetrofitModel[size];
        }
    };
}
