package com.shlomot.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;


public class BaseMovieModel implements Parcelable {
    public BaseMovieModel(String title, String image, Double rating, Integer releaseYear) {
        this.title = title;
        this.image = image;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }

    @ColumnInfo(name = "title")
    @SerializedName("title")
    @NotNull
    private String title;

    @ColumnInfo(name = "image")
    @SerializedName("image")
    private String image;

    @SerializedName("rating")
    private Double rating;
    @SerializedName("releaseYear")
    private Integer releaseYear;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.image);
        dest.writeValue(this.rating);
        dest.writeValue(this.releaseYear);
    }

    @Ignore
    protected BaseMovieModel(Parcel in) {
        this.title = in.readString();
        this.image = in.readString();
        this.rating = (Double) in.readValue(Double.class.getClassLoader());
        this.releaseYear = (Integer) in.readValue(Integer.class.getClassLoader());
    }

}
