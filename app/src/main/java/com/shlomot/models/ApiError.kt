package com.shlomot.models

class ApiError {
    private var statusCode: Int? = null
    private var endpoint: String? = null
    private var message: String = "Unknown Error"

    fun getStatusCode() : Int? {
        return statusCode
    }

    fun getEndpoint() : String? {
        return endpoint
    }

    fun getMessage() : String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }
}