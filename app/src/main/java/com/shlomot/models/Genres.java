package com.shlomot.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Ignore;

import java.util.List;

public class Genres implements Parcelable {
    // A wrapper used to enable room to store it as a json on the database

    public Genres(List<String> genres) {
        this.genres = genres;
    }

    List<String> genres;

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.genres);
    }

    @Ignore
    protected Genres(Parcel in) {
        this.genres = in.createStringArrayList();
    }

    @Ignore
    public static final Parcelable.Creator<Genres> CREATOR = new Parcelable.Creator<Genres>() {
        @Override
        public Genres createFromParcel(Parcel source) {
            return new Genres(source);
        }

        @Override
        public Genres[] newArray(int size) {
            return new Genres[size];
        }
    };
}
