package com.shlomot.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shlomot.models.Genres;

import java.lang.reflect.Type;

public class GenresConverter {


    @TypeConverter
    public static Genres fromString(String value) {
        Type features = new TypeToken<Genres>() {}.getType();
        return new Gson().fromJson(value, features);
    }

    @TypeConverter
    public static String fromObject(Genres gnr) {
        Gson gson = new Gson();
        String json = gson.toJson(gnr);
        return json;
    }
}
