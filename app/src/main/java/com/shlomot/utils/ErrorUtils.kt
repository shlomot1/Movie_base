package com.shlomot.utils

import com.shlomot.models.ApiError
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class ErrorUtils(retrofit: Retrofit) {
    private var retrofit: Retrofit

    init {
        this.retrofit = retrofit
    }

    fun parseError(response: Response<*>): ApiError {
        val converter : Converter<ResponseBody, ApiError> = retrofit.responseBodyConverter(ApiError::class.java, arrayOf())
        var error : ApiError? = ApiError()
        try {
            if (response.errorBody() != null) {
                error = converter.convert(response.errorBody())
            }
        } catch (e: IOException) {
            return ApiError()
        }
        return error!!
    }
}