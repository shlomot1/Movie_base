package com.shlomot.retrofit_components

import com.shlomot.models.MovieRetrofitModel
import retrofit2.Call
import retrofit2.http.GET

interface ServiceRequest {
    @GET("movies.json")
    fun getMovies(): Call<List<MovieRetrofitModel>>
}