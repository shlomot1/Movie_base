package com.shlomot.retrofit_components

import android.util.Log
import com.shlomot.interfaces.OnCompletionListener
import com.shlomot.models.ApiError
import com.shlomot.utils.ErrorUtils
import com.shlomot.models.MovieRetrofitModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Response
import retrofit2.Retrofit
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.annotations.NotNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.jvm.Throws

object RequestManager {
    private var retrofit: Retrofit? = null
    private var mErrorUtils: ErrorUtils? = null

    fun getRetrofitServiceRequest() : ServiceRequest {

        if (retrofit == null) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .connectTimeout(17, TimeUnit.SECONDS)
                .readTimeout(17, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addInterceptor(object : Interceptor{
                    @Throws(IOException::class)
                    override fun intercept(@NotNull chain: Interceptor.Chain): okhttp3.Response {
                        val original: Request = chain.request()
                        val request: Request = original.newBuilder()
                            .build()
                        return chain.proceed(request)
                    }
                })
                .build()
//            val BASE_URL =
            retrofit = Retrofit.Builder().baseUrl("https://api.androidhive.info/json/").addConverterFactory(GsonConverterFactory.create()).build()

            mErrorUtils = ErrorUtils(retrofit!!)
        }

        return retrofit!!.create(ServiceRequest::class.java)
    }

    fun getMovies(listener: OnCompletionListener<List<MovieRetrofitModel>>) {
        val call: Call<List<MovieRetrofitModel>> = getRetrofitServiceRequest().getMovies()

        call.enqueue(object : Callback<List<MovieRetrofitModel>> {

            override fun onResponse(call: Call<List<MovieRetrofitModel>>, response: Response<List<MovieRetrofitModel>>) {
                Log.d("test", "onResponse: ")
                var coroutineScope = CoroutineScope(Dispatchers.IO)
                coroutineScope.launch {
                if (response.isSuccessful && response.body() != null) {
                    listener.onSuccess(response.body()!!)
                } else {
                    val error = mErrorUtils!!.parseError(response)
                    listener.onFailure(error)
                }
                }
            }

            override fun onFailure(call: Call<List<MovieRetrofitModel>>, t: Throwable) {
                Log.d("test", "onFailure: ")
                /*var coroutineScope = CoroutineScope(Dispatchers.IO)
                coroutineScope.launch {*/
                val error = ApiError()
                error.setMessage("Something went wrong :(")
                listener.onFailure(error)
            }
//            }
        })
    }


}