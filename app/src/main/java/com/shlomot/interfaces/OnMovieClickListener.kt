package com.shlomot.interfaces

import com.shlomot.models.MovieRoomModel

interface OnMovieClickListener {
    fun onMovieClicked(movies: ArrayList<MovieRoomModel>?, index: Int)
}