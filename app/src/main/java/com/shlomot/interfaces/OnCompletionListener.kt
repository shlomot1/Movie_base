package com.shlomot.interfaces

import com.shlomot.models.ApiError

interface OnCompletionListener<T> {
    fun onSuccess(result: T)

    fun onFailure(error: ApiError)

}