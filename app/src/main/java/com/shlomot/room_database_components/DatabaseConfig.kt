package com.shlomot.room_database_components

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.shlomot.models.MovieRoomModel

@Database(entities = [MovieRoomModel::class], version = 1)
abstract class DatabaseConfig : RoomDatabase() {

    abstract fun movieDao(): MovieDao?

    companion object {
        private const val MOVIE_BASE = "MOVIE_BASE"
        private var instance: DatabaseConfig? = null

        @JvmStatic
        fun getInstance(context: Context): DatabaseConfig? {
            if (instance == null) {

                synchronized(DatabaseConfig::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        DatabaseConfig::class.java, MOVIE_BASE)
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                }
            }

            return instance

        }

        fun destroyDataBase(){
            instance = null
        }

    }

}
