package com.shlomot.room_database_components

import android.app.Application
import androidx.lifecycle.LiveData
import com.shlomot.models.MovieRoomModel
import com.shlomot.room_database_components.MovieDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class MoviesRepository(application: Application) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var movieDao: MovieDao?

    init {
        val db = DatabaseConfig.getInstance(application)
        movieDao = db?.movieDao()
    }

    fun getAllMovies(): LiveData<List<MovieRoomModel>> {
        return movieDao?.getAllMovies()!!
    }

    fun insert(movies: List<MovieRoomModel>) {
        launch {
            insertOnBG(movies)
        }
    }

    private suspend fun insertOnBG(movies: List<MovieRoomModel>) {
        withContext(Dispatchers.IO) {
            movieDao?.insert(movies)
        }
    }

}
