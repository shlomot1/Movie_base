package com.shlomot.room_database_components

import androidx.lifecycle.LiveData
import androidx.room.*
import com.shlomot.models.MovieRoomModel

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies: List<MovieRoomModel>)

    @Query("SELECT * FROM movie_table ORDER BY releaseYear DESC")
    fun getAllMovies(): LiveData<List<MovieRoomModel>>
}