package com.shlomot.room_database_components

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.shlomot.models.MovieRoomModel
import com.shlomot.room_database_components.MoviesRepository
import org.jetbrains.annotations.NotNull

class MovieViewModel(@NotNull application: Application) : AndroidViewModel(application) {

    private var repository: MoviesRepository
    private var allMovies: LiveData<List<MovieRoomModel>>

    init {
        this.repository = MoviesRepository(application)
        this.allMovies = repository.getAllMovies()
    }

    fun insert(movies: List<MovieRoomModel>) {
        repository.insert(movies)
    }

    fun getAllMovies(): LiveData<List<MovieRoomModel>> {
//        this.allMovies = repository.getAllMovies()
        return this.allMovies
    }

}