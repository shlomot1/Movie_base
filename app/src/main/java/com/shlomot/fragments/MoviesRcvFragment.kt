package com.shlomot.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shlomot.R
import com.shlomot.adapters.RcvAdapter
import com.shlomot.interfaces.OnMovieClickListener
import com.shlomot.models.MovieRoomModel
import com.shlomot.room_database_components.MovieViewModel
import kotlinx.android.synthetic.main.fragment_rcv_movies.view.*
import android.support.v4.app.*

class MoviesRcvFragment: Fragment(), OnMovieClickListener{
    var mListener : OnMovieClickListener? = null
    var movies: ArrayList<MovieRoomModel>? = null
    var mAdapter : RcvAdapter? = null
    lateinit var rcv : RecyclerView



    companion object {
        @JvmStatic
        fun newInstance() = MoviesRcvFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = context as OnMovieClickListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement onViewSelected")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_rcv_movies, container, false)

//        initRecyclerView(view)

        return view
    }


    private fun initRecyclerView(view: View) {
        rcv = view.FRM_rcv
        val layoutManager = GridLayoutManager(context,1)
        rcv.layoutManager = layoutManager
    }


    override fun onMovieClicked(movies: ArrayList<MovieRoomModel>?, index: Int) {
        mListener?.onMovieClicked(this.movies, index)
    }

    fun updateMovies(movies: ArrayList<MovieRoomModel>) {
//        withContext(Dispatchers.Main) {
            if (mAdapter == null && view != null) {
                initRecyclerView(requireView())
                mAdapter = RcvAdapter(movies,this)
                rcv.adapter = mAdapter
            } else {
                mAdapter!!.setMovies(movies)
            }
//        }
    }
}