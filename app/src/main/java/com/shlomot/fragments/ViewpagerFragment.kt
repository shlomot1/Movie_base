package com.shlomot.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.shlomot.R
import com.shlomot.models.MovieRoomModel
import kotlinx.android.synthetic.main.fragment_viewpager.view.*



class ViewpagerFragment : Fragment(){
    private val INDEX_KEY = "index_key"
    private var position: Int = 0
    private var movies: ArrayList<MovieRoomModel>? = java.util.ArrayList()
    private var pager: ViewPager? = null
    private var pagerAdapter: PagerAdapter? = null
    private var fragments: ArrayList<Fragment> = ArrayList()

    companion object {
        @JvmStatic
        fun newInstance(movies: ArrayList<MovieRoomModel>, index : Int) =
            ViewpagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(MovieRoomModel::class.java.simpleName, movies)
                    putInt(INDEX_KEY, index)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            this.position = requireArguments().getInt(INDEX_KEY)
            this.movies = requireArguments().getParcelableArrayList(MovieRoomModel::class.java.simpleName)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view: View = inflater.inflate(R.layout.fragment_viewpager, container, false)

        init(view)

        return view
    }

   fun updateMovies(movies: ArrayList<MovieRoomModel>) {
       this.movies = movies
   }

    private fun init(view: View) {
        pager = view.FV_pager
        if (!movies.isNullOrEmpty()) {
            loadFragments()
            initAdapter()
            bringPagerToPosition(position)
        }
    }

    private fun loadFragments() {
        for (i in movies!!.indices) {
            fragments.add(MovieDetailsFragment.newInstance(movies!![i] as MovieRoomModel))
        }
    }

    private fun initAdapter() {
        //init adapter
        pagerAdapter = SimplePagerAdapter(childFragmentManager, fragments)
        pager?.adapter = pagerAdapter
    }

    class SimplePagerAdapter(fm: FragmentManager, var fragments: List<Fragment>) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }
    }

    fun bringPagerToPosition(idx: Int) {
        pager?.setCurrentItem(idx)
    }
}