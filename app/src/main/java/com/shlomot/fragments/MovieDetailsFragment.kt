package com.shlomot.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.shlomot.R
import com.shlomot.models.MovieRoomModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details_movie.view.*

class MovieDetailsFragment : Fragment() {

    private var movie: MovieRoomModel? = null

    private var header : ImageView? =null
    private var poster : ImageView? =null
    private var title : TextView? =null
    private var rating : TextView? =null
    private var genre : TextView? =null
    private var date : TextView?= null

    companion object {
        @JvmStatic
        fun newInstance(movie: MovieRoomModel) =
            MovieDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(MovieRoomModel::class.java.simpleName, movie)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            movie = requireArguments().getParcelable(MovieRoomModel::class.java.simpleName)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews(view)

        init()
    }

    private fun findViews(view: View) {
        header = view.FDM_header
        poster = view.FDM_secImage
        title = view.FDM_title
        rating = view.FDM_rating
        genre = view.FDM_genre
        date = view.FDM_released
    }

    private fun init() {
        if (movie == null) return
        title?.text = movie?.title
        rating?.text = movie?.rating.toString()
        date?.text = movie?.releaseYear.toString()
        genre?.text = movie?.genres?.genres?.joinToString(separator = " | ")

        Picasso.get()
            .load(movie!!.image)
            .fit().centerCrop()
            .into(header)

        Picasso.get()
            .load(movie!!.image)
            .fit().centerCrop()
            .into(poster)


    }
}
